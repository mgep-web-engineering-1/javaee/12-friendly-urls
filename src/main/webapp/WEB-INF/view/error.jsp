<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Error page</title>
</head>

<body>
  <h1>'filter-me' Error</h1>
  <p>You are trying to access a path with filter-me in the URL. It is not allowed.</p>
</body>

</html>