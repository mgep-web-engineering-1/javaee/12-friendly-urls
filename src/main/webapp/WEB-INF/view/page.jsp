<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Page</title>
  <link rel="stylesheet" href="/css/style.css">
</head>

<body>
  <a href='/'>Go back</a><br />
  <table>
    <thead>
      <tr>
        <th>ID</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <c:out value="${requestScope.id}" />
        </td>
        <td>
          <c:out value="${requestScope.action}" />
        </td>
      </tr>
    </tbody>
  </table>
</body>

</html>