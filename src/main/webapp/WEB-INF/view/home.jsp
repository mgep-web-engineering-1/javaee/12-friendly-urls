<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Friendly URLS</title>
  </head>

  <body>
    <p>This page is showing home.jsp</p>
    <ul>
      <li>
        Friendly URLs
        <ul>
          <li><a href="/friendly">'empty' (friendly)</a></li>
          <li><a href="/friendly/">/ (friendly/)</a></li>
          <li><a href="/friendly/create">Just action (friendly/create)</a></li>
          <li><a href="/friendly/1">Just id (friendly/1)</a></li>
          <li><a href="/friendly/1/edit">Id + action (friendly/1/edit)</a></li>
        </ul>
      </li>
      <li><a href="/WEB-INF/view/home.jsp">Try to access home.jsp</a> (will return a 404 error)</li>
    </ul>
  </body>

  </html>