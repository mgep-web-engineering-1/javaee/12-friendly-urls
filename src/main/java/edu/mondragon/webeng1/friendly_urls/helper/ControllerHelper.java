package edu.mondragon.webeng1.friendly_urls.helper;

import jakarta.servlet.http.HttpServletRequest;

/**
 * This class will split the URL and extract the element ID and the action
 * String. It can be reused in multiple controllers.
 * 
 * @author aperez
 *
 */
public class ControllerHelper {
    private String[] pathElements;
    private String action = "list";
    private int id = -1;

    public ControllerHelper(HttpServletRequest request) {
        // Constructor
        String pathInfo = request.getPathInfo();
        
        if (pathInfo != null && !pathInfo.contentEquals("/")) {
            System.out.println("Path info:" + pathInfo);
            pathElements = pathInfo.replaceFirst("/", "").split("/");
            getIdFromPathElements();
            getActionFromPathElements();
        }
        /*
        * else { // e.g. http://localhost:8080/FriendlyURL/friendly or
        * http://localhost:8080/FriendlyURL/friendly/ //For empty path, default values
        * => list & -1 }
        */
    }

    private void getIdFromPathElements() {
        try {
            id = Integer.parseInt(pathElements[0]);
        } catch (NumberFormatException e) {
            /*
             * If pathElements[0] is not an integer, request does not have an ID.
             * default value will be used (id = -1)
             */
        }
    }

    private void getActionFromPathElements() {
        if (pathElements.length > 1) {
            // Both id & action have been sent
            action = pathElements[1];
            return;
        }
        
        // Id or action has not been sent.
        if (id == -1) {
            /*
             * If id==-1, no id has been sent => first element must be an action.
             * e.g. http://localhost:8080/FriendlyURL/friendly/create
             */
            action = pathElements[0];
            return;
        }

        /* 
         * If the id is an integer and no action is sent, view the element.
         * e.g. http://localhost:8080/FriendlyURL/friendly/1
         */
        action = "view";
    }

    public String getAction() {
        return action;
    }

    public int getId() {
        return id;
    }
}
