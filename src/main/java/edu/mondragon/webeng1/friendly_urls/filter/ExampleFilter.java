package edu.mondragon.webeng1.friendly_urls.filter;

import java.io.IOException;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
//import jakarta.servlet.http.HttpSession;

/**
 * This class will filter any path that contains 'filter-me'.
 * 
 * @author aperez
 *
 */
@WebFilter("/*")
public class ExampleFilter implements Filter {

    public ExampleFilter() {
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        System.out.println("doFilter function");
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        // HttpSession session = request.getSession(false);

        String requestPath = request.getRequestURI();
        System.out.println(requestPath);

        if (requestPath.contains("filter-me")) {
            System.out.println("Call filtered. 'filter-me' string in the path.");
            response.sendRedirect("error");
        } else {
            System.out.println("No 'filter-me' string in the path, continue normally.");
            chain.doFilter(req, res);
        }
    }

    public void init(FilterConfig fConfig) throws ServletException {
    }

}
