package edu.mondragon.webeng1.friendly_urls.controller;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import edu.mondragon.webeng1.friendly_urls.helper.ControllerHelper;

@WebServlet(name = "FriendlyController", urlPatterns = { "/friendly/*" })
public class FriendlyController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public FriendlyController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Friendly URL Controller Helper will split the url and return the element ID
        // and the action String.
        ControllerHelper controllerHelper = new ControllerHelper(request);
        int id = controllerHelper.getId();
        String action = controllerHelper.getAction();

        request.setAttribute("id", id);
        request.setAttribute("action", action);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/page.jsp");
        dispatcher.forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
