package edu.mondragon.webeng1.friendly_urls.controller;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * This class will show the error page.
 * 
 * @author aperez
 *
 */
@WebServlet("/error")
public class ErrorController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ErrorController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Error Controller");
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/error.jsp");
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
