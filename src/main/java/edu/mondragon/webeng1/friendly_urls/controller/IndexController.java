package edu.mondragon.webeng1.friendly_urls.controller;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * This class will show the main page (as index_page.jsp is not directly accessible).
 * Be careful, becouse an index.jsp file will make this controller unaccessible.
 * @author aperez
 *
 */
@WebServlet( name = "IndexController", urlPatterns = {"/index.html"})
public class IndexController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public IndexController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Index Controller");
		RequestDispatcher dispatcher = getServletContext()
        .getRequestDispatcher("/WEB-INF/view/home.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
